Source: dropbear
Section: net
Priority: optional
Maintainer: Guilhem Moulin <guilhem@debian.org>
Uploaders: Gerrit Pape <pape@smarden.org>
Build-Depends: debhelper (>= 9),
               dh-autoreconf,
               dh-exec,
               libtomcrypt-dev,
               libtommath-dev,
               libz-dev
Standards-Version: 4.1.4
Homepage: https://matt.ucc.asn.au/dropbear/dropbear.html
Vcs-Git: https://salsa.debian.org/debian/dropbear.git
Vcs-Browser: https://salsa.debian.org/debian/dropbear

Package: dropbear-bin
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: openssh-client, xauth
Breaks: dropbear (<< 2015.68-1)
Replaces: dropbear (<< 2015.68-1)
Provides: ssh-server
Multi-Arch: foreign
Description: lightweight SSH2 server and client - command line tools
 dropbear is a SSH 2 server and client designed to be small enough to
 be used in small memory environments, while still being functional and
 secure enough for general use.
 .
 It implements most required features of the SSH 2 protocol, and other
 features such as X11 and authentication agent forwarding.
 .
 This package provides dropbear, dbclient, dropbearkey and dropbearconvert.

Package: dropbear-run
Architecture: all
Depends: dropbear-bin (>= ${source:Version}),
         lsb-base (>= 3.0-6),
         ${misc:Depends}
Suggests: runit
Breaks: dropbear (<< 2015.68-1)
Replaces: dropbear (<< 2015.68-1)
Multi-Arch: foreign
Description: lightweight SSH2 server and client - startup scripts
 dropbear is a SSH 2 server and client designed to be small enough to
 be used in small memory environments, while still being functional and
 secure enough for general use.
 .
 It implements most required features of the SSH 2 protocol, and other
 features such as X11 and authentication agent forwarding.

Package: dropbear-initramfs
Architecture: all
Depends: busybox | busybox-static,
         dropbear-bin (>= ${source:Version}),
         initramfs-tools (>= 0.94),
         udev,
         ${misc:Depends}
Recommends: cryptsetup
Breaks: dropbear (<< 2015.68-1)
Replaces: dropbear (<< 2015.68-1)
Multi-Arch: foreign
Description: lightweight SSH2 server and client - initramfs integration
 dropbear is a SSH 2 server and client designed to be small enough to
 be used in small memory environments, while still being functional and
 secure enough for general use.
 .
 It implements most required features of the SSH 2 protocol, and other
 features such as X11 and authentication agent forwarding.
 .
 This package provides initramfs integration.

Package: dropbear
Architecture: all
Section: oldlibs
Pre-Depends: dpkg (>= 1.17.14)
Depends: dropbear-initramfs (>= 2015.68-1),
         dropbear-run (>= 2015.68-1),
         ${misc:Depends}
Multi-Arch: foreign
Description: transitional dummy package for dropbear-{run,initramfs}
 This is a transitional dummy package to get upgrading systems to install the
 dropbear-run and dropbear-initramfs packages. It can safely be removed once no
 other package depends on it.
